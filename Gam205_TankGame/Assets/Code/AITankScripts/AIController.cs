﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    private TankMotor tankMotor;
    private TankData tankData;
    private TankCannon tankCannon;

    // Start is called before the first frame update
    void Start()
    {
        tankMotor = GetComponent<TankMotor>();
        tankData = GetComponent<TankData>();
        tankCannon = GetComponent<TankCannon>();
        //set health on start
        tankData.Health = tankData.maxHealth;       
    }

    // Update is called once per frame
    void Update()
    {
        //placeholder
        tankMotor.Move(0);
        //CheckIfTargetNearby();
        TargetInRange();
    }

    void CheckIfTargetNearby()
    {

        RaycastHit hit;
        Vector3 originPoint = transform.position + gameObject.GetComponent<CharacterController>().center;
        if(Physics.SphereCast(originPoint, gameObject.GetComponent<CharacterController>().height/2, transform.forward, out hit, tankData.detectDistance))
        {
            //rotate to look at object
            //tankMotor.Rotate()
        }
    }

    //a raycast function to detect if there is a target in range to shoot
    void TargetInRange()
    {
        //raycast variables
        Vector3 originPoint = tankData.cannonPoint.transform.position;
        Vector3 rayDirection = tankData.cannonPoint.transform.forward;
        RaycastHit hit;
        //raycast based on variables
        if(Physics.Raycast(originPoint, rayDirection, out hit, tankData.shootDistance))
        {
            //check if the hit target has tank data
            if(hit.collider.gameObject.GetComponent<TankData>())
            {
                //call the fire function on the cannon
                tankCannon.FireShell(tankData.cannonFireRate);
            }
        }
    }
}
