﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShellMover : MonoBehaviour
{
    public float shellDamage;
    public float pointsToGive;
    public float shellLifetime;
    public GameObject parentTank; 

    //collision detection
    private void OnCollisionEnter(Collision other)
    {
        //checking if collision object is AI
        if (other.gameObject.GetComponent<TankData>())
        {
            //Do damage object
            other.gameObject.GetComponent<TankData>().Health -= shellDamage;           
            //giving points if collision object dies
            if (other.gameObject.GetComponent<TankData>().Health <= 0)
            {
                parentTank.GetComponent<TankData>().TankScore += pointsToGive;
            }
            //destroy shell
            gameObject.SetActive(false);
        }
        else
        { 
            //destroy shell
            gameObject.SetActive(false);
        }
    }

    //couroutine to deactivate shell after time
    public IEnumerator DeactivateShell()
    {
        yield return new WaitForSeconds(shellLifetime);
        gameObject.SetActive(false);
    }
}
