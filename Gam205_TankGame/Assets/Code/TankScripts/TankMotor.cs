﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour
{
    private CharacterController cc;
    private Transform tf;

    private void Awake()
    {
        cc = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
    }

    //move function that takes in speed * direction to designate move direction
    public void Move(float speed)
    {
        Vector3 speedVector;
        speedVector = tf.forward * speed;
        cc.SimpleMove(speedVector);
    }

    //rotate function that takes in speed * direction to designate rotation direction
    public void Rotate(float speed)
    {
        Vector3 rotateVector;
        rotateVector = Vector3.up * speed * Time.deltaTime;
        tf.Rotate(rotateVector, Space.Self);
    }
}
