﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    private TankMotor tM;
    private TankData data;
    private TankCannon cannon;

    // Start is called before the first frame update
    void Start()
    {
        tM = GetComponent<TankMotor>();
        data = GetComponent<TankData>();
        cannon = GetComponent<TankCannon>();
        //set health on start
        data.Health = data.maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        //input for tank movement using Axis
        if (Input.GetAxis("Vertical") > 0)
        {
            tM.Move(Input.GetAxis("Vertical") * data.tankForwardSpeed);
        }
        else { tM.Move(Input.GetAxis("Vertical") * data.tankReverseSpeed); }
        tM.Rotate(Input.GetAxis("Horizontal") * data.tankRotateSpeed);

        //calls the shooter function
        TankShooter();
    }

    //shooter function to detect input
    void TankShooter()
    {
        //if space is pressed run cannon shoot function
        if (Input.GetKey(KeyCode.Space))
        {
            cannon.FireShell(data.cannonFireRate);
        }
    }
}
