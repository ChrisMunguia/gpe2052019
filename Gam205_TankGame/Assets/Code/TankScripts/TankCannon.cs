﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCannon : MonoBehaviour
{
    private float lastShot;
    private TankData data;

    // Start is called before the first frame update
    void Start()
    {
        data = GetComponent<TankData>();
    }

    //shoot function that grabs shell from pool and sets data of shell once instantiated
    public void FireShell(float fireRate)
    {
        //fire rate statement
        if (Time.time > fireRate + lastShot)
        {
            //shell instantiater
            GameObject shell = ShellPooler.SharedInstance.GetPooledObject();
            if (shell != null)
            {
                //set all shell data
                shell.GetComponent<TankShellMover>().shellDamage = data.shellDamage;
                shell.GetComponent<TankShellMover>().pointsToGive = data.pointsOnDeath;
                shell.GetComponent<TankShellMover>().parentTank = gameObject;
                shell.transform.position = data.cannonPoint.transform.position;
                shell.transform.rotation = data.cannonPoint.transform.rotation;
                shell.SetActive(true);
                shell.GetComponent<Rigidbody>().velocity = Vector3.zero;
                shell.GetComponent<Rigidbody>().AddForce(data.cannonPoint.transform.forward * data.shellSpeed, ForceMode.Force);
                StartCoroutine(shell.GetComponent<TankShellMover>().DeactivateShell());
            }
            lastShot = Time.time;
        }
    }
}
