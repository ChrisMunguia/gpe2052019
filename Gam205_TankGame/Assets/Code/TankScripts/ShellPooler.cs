﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellPooler : MonoBehaviour
{
    public static ShellPooler SharedInstance;
    public List<GameObject> tankShell = new List<GameObject>();
    public GameObject shellObject;
    public int shellAmount;

    private void Awake()
    {
        SharedInstance = this;
    }

    private void Start()
    {
        //spawn tank shells as children of tank
        for (int i = 0; i < shellAmount; i++)
        {
            GameObject shell = Instantiate(shellObject);
            shell.SetActive(false);
            tankShell.Add(shell);
            shell.transform.parent = gameObject.transform;
        }
    }

    public GameObject GetPooledObject()
    {
        for(int i = 0; i < tankShell.Count; i++)
        {
            if (!tankShell[i].activeInHierarchy)
            {
                return tankShell[i];
            }
        }
        return null;
    }
}
