﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankData : MonoBehaviour
{
    [Header("Tank Score")]
    [SerializeField]
    private float tankScore;
    //score property tracks score
    public float TankScore {
        get { return tankScore; }
        set {
            //sets score and checks for win condition
            tankScore = value;
            if(tankScore >= 100)
            {
                //player win stuff
            }
        }
    }

    [Header("Points Earned On Death")]
    public float pointsOnDeath;

    [Header("Tank Speed Data")]
    public float tankForwardSpeed;
    public float tankReverseSpeed;
    public float tankRotateSpeed;

    [Header("Cannon Data")]
    public float cannonFireRate;
    public GameObject cannonPoint;

    [Header("Shell Data")]
    public float shellSpeed;
    public float shellDamage;

    [Header("Tank Health")]
    public float maxHealth;
    public Slider playerHealthUI;
    [SerializeField]
    private float currentHealth;
    //health property tracks health
    public float Health {
        get {
            return currentHealth;
        }
        set {
            currentHealth = value;
            //set healthbar here
            if(playerHealthUI != null)
            playerHealthUI.value = Health / 100;
            //check to see if players health is below 0
            if (currentHealth <= 0)
            {
                //deactivate
                gameObject.SetActive(false);
            }         
        }
    }

    [Header("AI Data")]
    public int detectDistance;
    public int shootDistance;
}
